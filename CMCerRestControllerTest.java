/**
 *
 */
package com.cmc.mockproject1.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.cmc.mockproject1.entity.CMCer;
import com.cmc.mockproject1.service.CMCerService;
import com.cmc.mockproject1.util.ConstantPage;
import com.google.gson.Gson;

/**
 * @author NATUAN3
 * @createddate Nov 25, 2018
 * @modifieddate Nov 25, 2018
 * @verson 1.0
 * @description
 * @return
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = CMCerRestAPIController.class)
public class CMCerRestControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CMCerService cmcerService;

	@InjectMocks
	private CMCerRestAPIController cmcerRestApiController;

	CMCer c1;
	CMCer c2;
	CMCer c3;
	CMCer c4;
	CMCer c5;
	CMCer c6;

	List<CMCer> listCMCers;

	/**
	 * @author NATUAN3
	 * @createddate Nov 25, 2018
	 * @modifieddate Nov 25, 2018
	 * @verson 1.0
	 * @description
	 * @return
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		listCMCers = new ArrayList<CMCer>();
		c1 = new CMCer(1, "Hello World", "background.jpg", "icon.jpg", "Nguyen Van A", "Manager", "Khong co gi", false);
		c2 = new CMCer(2, "Hello World", "background1.jpg", "icon1.jpg", "Nguyen Van B", "Manager", "Khong co gi",
				true);
		c3 = new CMCer(3, "Hello World", "background2.jpg", "icon2.jpg", "Nguyen Van C", "Manager", "Khong co gi",
				true);
		c4 = new CMCer(4, "Hello World", "background3.jpg", "icon3.jpg", "Nguyen Van D", "Manager", "Khong co gi",
				true);
		c5 = new CMCer(5, "Hello World", "background4.jpg", "icon4.jpg", "Nguyen Van E", "Manager", "Khong co gi",
				false);
		c6 = new CMCer(6, "Hello World", "background5.jpg", "icon5.jpg", "Nguyen Van F", "Manager", "Khong co gi",
				false);
		listCMCers.add(c1);
		listCMCers.add(c2);
		listCMCers.add(c3);
		listCMCers.add(c4);
		listCMCers.add(c5);
		listCMCers.add(c6);
	}

	/**
	 * @author NATUAN3
	 * @createddate Nov 25, 2018
	 * @modifieddate Nov 25, 2018
	 * @verson 1.0
	 * @description
	 * @return
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetListCMCer() {
		Mockito.when(cmcerRestApiController.getListAllCMCer()).thenReturn(listCMCers);
		String json = new Gson().toJson(listCMCers);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(ConstantPage.REST_API_URL + ConstantPage.REST_API_GET_ALL_CMCERS)
				.accept(MediaType.APPLICATION_PROBLEM_JSON_VALUE);
		try {
			MvcResult result = mockMvc.perform(requestBuilder).andReturn();

			assertEquals(json, result.getResponse().getContentAsString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
    public void testGetCMCerByTitle() {
    	List<CMCer> listFilter = new ArrayList<CMCer>();
    	listFilter.add(c1);
    	Mockito.when(cmcerRestApiController.getListFilterCMCer(Mockito.anyString())).thenReturn(listFilter);
    	RequestBuilder requestBuilder = MockMvcRequestBuilders.get(ConstantPage.REST_API_URL + ConstantPage.REST_API_FILTER_CMCERS_BY_TITLE, "c")
                .accept(MediaType.APPLICATION_PROBLEM_JSON_UTF8_VALUE);
		try {
			mockMvc.perform(requestBuilder).andExpect(status().isOk())
        	.andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON_UTF8_VALUE))
        	.andExpect(jsonPath("$[0].title", is("Hello World")));
		} catch (Exception e) {
			e.printStackTrace();
		}

    }
	@Test
    public void testGetListCMCerActive() {
    	List<CMCer> listActive = new ArrayList<CMCer>();
    	listActive.add(c2);
    	listActive.add(c3);
    	listActive.add(c4);
    	Mockito.when(cmcerRestApiController.getListCMCerActive()).thenReturn(listActive);
        String json = new Gson().toJson(listActive);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get
        		(ConstantPage.REST_API_URL + ConstantPage.REST_API_GET_ALL_CMCERS_ACTIVE)
                .accept(MediaType.APPLICATION_PROBLEM_JSON_VALUE);
        try {
            MvcResult result = mockMvc.perform(requestBuilder).andReturn();

            assertEquals(json, result.getResponse().getContentAsString());

            } catch (Exception e) {
            e.printStackTrace();
        }
    }
	@Test
    public void testDeleteCMCer() {
        int id = 2;
        Mockito.when(cmcerRestApiController.deleteCMCer(id)).thenReturn(true);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(ConstantPage.REST_API_URL + ConstantPage.REST_API_DELETE_CMCERS_BY_ID, id)
                .accept(MediaType.APPLICATION_PROBLEM_JSON_VALUE);
        try {
            MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
            int status = mvcResult.getResponse().getStatus();
               assertEquals(200, status);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	@Test
    public void testInsertCMCer() {
        MockMultipartFile image = new MockMultipartFile("file", "background.jpg", "image/jpeg", "some image".getBytes() );
        MockMultipartFile iconImage = new MockMultipartFile("file", "icon.jpg", "image/jpeg", "some image".getBytes() );
        CMCer c = new CMCer();
        c.setTitle("Title test");
        c.setEmployeeName("Emp name test");
        c.setEmployeePosition("Position test");
        c.setComment("Comment test");
        c.setImg(image.getOriginalFilename());
        c.setIconImg(iconImage.getOriginalFilename());
        String cmcer  = new Gson().toJson(c);

        try {
			mockMvc.perform(MockMvcRequestBuilders.multipart
					(ConstantPage.REST_API_URL + ConstantPage.REST_API_INSERT_CMCERS)
					.file("image", image.getBytes())
					.file("iconImage", iconImage.getBytes())
					.param("cmcer", cmcer)).
			andExpect(status().is(200));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
	@Test
    public void testUpdateCMCer() {
        MockMultipartFile image = new MockMultipartFile("file", "background.jpg", "image/jpeg", "some image".getBytes() );
        MockMultipartFile iconImage = new MockMultipartFile("file", "icon.jpg", "image/jpeg", "some image".getBytes() );
        c2.setTitle("Title test");
        c2.setEmployeeName("Emp name test");
        c2.setEmployeePosition("Position test");
        c2.setComment("Comment test");
        c2.setImg(image.getOriginalFilename());
        c2.setIconImg(iconImage.getOriginalFilename());
        String cmcer  = new Gson().toJson(c2);

        try {
			mockMvc.perform(MockMvcRequestBuilders.multipart(ConstantPage.REST_API_URL +
					ConstantPage.REST_API_UPDATE_CMCERS)
					.file("image", image.getBytes())
					.file("iconImage", iconImage.getBytes())
					.param("cmcer", cmcer)).
			andExpect(status().is(200));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	@Test
	public void testUpdateCMCerStatus() {
		boolean isShow = true;
		c6.setShow(isShow);
		String cmcer = new Gson().toJson(c6);
		try {
			mockMvc.perform(MockMvcRequestBuilders.post(ConstantPage.REST_API_URL +
					ConstantPage.REST_API_UPDATE_CMCERS_ACTIVE)
					.param("cmcer", cmcer)).
			andExpect(status().is(200));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
