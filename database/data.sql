-- insert user table
INSERT INTO `testing_system_database`.`user` (`user_id`, `full_name`, `email`, `mobile`, `password`, `status`)
 VALUES ( 1, 'Vu Van Dong', 'vudongkm1997@gmail.com', '0332132609', '123', 1);
INSERT INTO `testing_system_database`.`user` (`user_id`, `full_name`, `email`, `mobile`, `password`, `status`)
 VALUES ( 2, 'Vuong Sy Son', 'son@gmail.com', '0332132609', '123', 1);
INSERT INTO `testing_system_database`.`user` (`user_id`, `full_name`, `email`, `mobile`, `password`, `status`)
 VALUES ( 3, 'Pham Thi Phuong', 'phuong@gmail.com', '0332132609', '123', 1);
INSERT INTO `testing_system_database`.`user` (`user_id`, `full_name`, `email`, `mobile`, `password`, `status`)
 VALUES ( 4, 'Nguyen Thi My Duyen', 'duyen@gmail.com', '0332132609', '123', 1);

-- insert category table
INSERT INTO `testing_system_database`.`category` (`category_id`, `category_name`, `user_id_created`, `date_created`, `status`)
 VALUES ( 1, 'Java', 1, '2019-02-10', 1);
INSERT INTO `testing_system_database`.`category` (`category_id`, `category_name`, `user_id_created`, `date_created`, `status`)
 VALUES ( 2, 'C#', 2, '2019-02-10', 1);
INSERT INTO `testing_system_database`.`category` (`category_id`, `category_name`, `user_id_created`, `date_created`, `status`)
 VALUES ( 3, 'SQL', 3, '2019-02-10', 1);
INSERT INTO `testing_system_database`.`category` (`category_id`, `category_name`, `user_id_created`, `date_created`, `status`)
 VALUES ( 4, 'Angular', 4, '2019-02-10', 1);
 
-- insert exam table
INSERT INTO `testing_system_database`.`exam` (`exam_id`, `title`, `duration`, `category_id`, `note`, `status`, `is_enable`, `number_of_question`, `create_at`, `create_by`, `modified_at`, `modified_by`)
 VALUES ('Exam001', 'Java basic', 30, 1, 'no note', 'Draft', 1, 10, '2019-02-15', 1, null, null);
INSERT INTO `testing_system_database`.`exam` (`exam_id`, `title`, `duration`, `category_id`, `note`, `status`, `is_enable`, `number_of_question`, `create_at`, `create_by`, `modified_at`, `modified_by`)
 VALUES ('Exam002', 'C# basic', 40, 2, 'no note', 'Draft', 1, 10, '2019-02-14', 2, null, null);
INSERT INTO `testing_system_database`.`exam` (`exam_id`, `title`, `duration`, `category_id`, `note`, `status`, `is_enable`, `number_of_question`, `create_at`, `create_by`, `modified_at`, `modified_by`)
 VALUES ('Exam003', 'SQL basic', 20, 3, 'no note', 'Draft', 1, 10, '2019-02-13', 3, null, null);
INSERT INTO `testing_system_database`.`exam` (`exam_id`, `title`, `duration`, `category_id`, `note`, `status`, `is_enable`, `number_of_question`, `create_at`, `create_by`, `modified_at`, `modified_by`)
 VALUES ('Exam004', 'Angular', 50, 4, 'no note', 'Draft', 1, 10, '2019-02-12', 4, null, null);


 -- insert question type table
 
INSERT INTO `testing_system_database`.`question_type` (`type_id`, `type_name`, `status`) VALUES (1, 'Multi choice', 1);
 
 -- insert question level table
 
INSERT INTO `testing_system_database`.`question_level` (`level_id`, `level_name`, `status`) VALUES (1, 'Normal', 1);
 
 -- insert tag table
 
INSERT INTO `testing_system_database`.`tag` (`tag_id`, `tag_name`, `status`) VALUES (1, '#JAVA', 1);
INSERT INTO `testing_system_database`.`tag` (`tag_id`, `tag_name`, `status`) VALUES (2, '#C#', 1);
INSERT INTO `testing_system_database`.`tag` (`tag_id`, `tag_name`, `status`) VALUES (3, '#SQL', 1);
INSERT INTO `testing_system_database`.`tag` (`tag_id`, `tag_name`, `status`) VALUES (4, '#ANGULAR', 1);



-- insert question table
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q001', 1, 'What is the range of short data type in Java?', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q002', 1, 'What is the range of byte data type in Java?', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q003', 1, 'An expression involving byte, int, and literal numbers is promoted to which of these?', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q004', 1, 'Which of these literals can be contained in float data type variable?', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q005', 1, 'Which data type value is returned by all transcendental math functions?', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q006', 1, 'What is the numerical range of a char data type in Java?', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
 INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q007', 1, 'Which of these coding types is used for data type characters in Java?', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q008', 1, 'Which of these values can a boolean variable contain?', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q009', 1, 'Which of these occupy first 0 to 127 in Unicode character set used for characters in Java?', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q010', 1, 'Which one is a valid declaration of a boolean?', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');

INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q011', 2, 'How many Bytes are stored by ‘Long’ Data type in C# .net?', 1, 1, 'no sugguestion', 2, 1, 2, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q012', 2, 'Choose “.NET class” name from which data type “UInt” is derived ?', 1, 1, 'no sugguestion', 2, 1, 2, '2019-02-10');
 INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q013', 2, 'Correct Declaration of Values to variables ‘a’ and ‘b’?', 1, 1, 'no sugguestion', 2, 1, 2, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q014', 2, 'Arrange the following data type in order of increasing magnitude sbyte, short, long, int.', 1, 1, 'no sugguestion', 2, 1, 2, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q015', 2, 'Which data type should be more preferred for storing a simple number like 35 to improve execution speed of a program?', 1, 1, 'no sugguestion', 2, 1, 2, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q016', 2, 'Valid Size of float data type is ?', 1, 1, 'no sugguestion', 2, 1, 2, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q017', 2, 'Correct way to assign values to variable ‘c’ when int a=12, float b=3.5,int c;', 1, 1, 'no sugguestion', 2, 1, 2, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q018', 2, 'Default Type of number without decimal is?', 1, 1, 'no sugguestion', 2, 1, 2, '2019-02-10');
 INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q019', 2, 'Select a convenient declaration and initialization of a floating point number:', 1, 1, 'no sugguestion', 2, 1, 2, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q020', 2, 'Number of digits upto which precision value of float data type is valid ?', 1, 1, 'no sugguestion', 2, 1, 2, '2019-02-10');
 
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q021', 3, 'Which SQL function is used to count the number of rows in a SQL query ?', 1, 1, 'no sugguestion', 3, 1, 3, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q022', 3, 'Which SQL keyword is used to retrieve a maximum value ?', 1, 1, 'no sugguestion', 3, 1, 3, '2019-02-10');
 INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q023', 3, 'Which of the following SQL clauses is used to DELETE tuples from a database table ?', 1, 1, 'no sugguestion', 3, 1, 3, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q024', 3, '___________removes all rows from a table without logging the individual row deletions.', 1, 1, 'no sugguestion', 3, 1, 3, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q025', 3, 'Which of the following is not a DDL command ?', 1, 1, 'no sugguestion', 3, 1, 3, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q026', 3, 'Which of the following are TCL commands ?', 1, 1, 'no sugguestion', 3, 1, 3, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q027', 3, '________________ is not a category of SQL command.', 1, 1, 'no sugguestion', 3, 1, 3, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q028', 3, 'If you don’t specify ASC or DESC after a SQL ORDER BY clause, the following is used by default', 1, 1, 'no sugguestion', 3, 1, 3, '2019-02-10');
 INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q029', 3, 'Which of the following statement is true ?', 1, 1, 'no sugguestion', 3, 1, 3, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q030', 3, 'What is the purpose of the SQL AS clause ?', 1, 1, 'no sugguestion', 3, 1, 3, '2019-02-10'); 
 
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q031', 4, 'Angular Js is based on', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q032', 4, 'AngularJS expressions are written using', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
 INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q033', 4, 'What is correct way to apply multiple filters in AngularJs.', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q034', 4, 'Which directive initializes an AngularJS application?', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q035', 4, 'Which of following is not valid AngularJs Filter', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q036', 4, 'Which Angular directive is used to binds the value of HTML controls (input, select, textarea) to application data?', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q037', 4, 'How do you share data between controller and view?', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q038', 4, 'AngularJS directives are used in ____________', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
 INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q039', 4, 'AngularJS directives can be written in Templates as', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10');
INSERT INTO `testing_system_database`.`question` (`question_id`, `category_id`, `content`, `type_id`, `level_id`, `sugguestion`, `tag_id`, `status`, `user_id_created`, `date_created`)
 VALUES ('Q040', 4, 'Which of the following directive allows us to use a form in AngularJs?', 1, 1, 'no sugguestion', 1, 1, 1, '2019-02-10'); 
 

 
 -- insert answer table
 
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer1', '-128 to 127', 0, 1, 'Q001');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer2', '-32768 to 32767', 1, 1, 'Q001');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer3', '-2147483648 to 2147483647', 0, 1, 'Q001');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer4', 'None of the mentioned', 0, 1, 'Q001');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer5', '-128 to 127', 1, 1, 'Q002');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer6', '-32768 to 32767', 0, 1, 'Q002');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer7', '-2147483648 to 21474836478', 0, 1, 'Q002');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer8', 'None of the mentioned', 0, 1, 'Q002');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer9', 'int', 1, 1, 'Q003');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer10', 'long', 0, 1, 'Q003');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer11', 'byte', 0, 1, 'Q003');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer12', 'float', 0, 1, 'Q003');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer13', '-1.7e+308', 0, 1, 'Q004');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer14', '-3.4e+038', 1, 1, 'Q004');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer15', '+1.7e+308', 0, 1, 'Q004');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer16', '-3.4e+050', 0, 1, 'Q004');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer17', 'int', 0, 1, 'Q005');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer18', 'float', 0, 1, 'Q005');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer19', 'double', 1, 1, 'Q005');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer20', 'long', 0, 1, 'Q005');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer21', '-128 to 127', 0, 1, 'Q006');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer22', '0 to 256', 0, 1, 'Q006');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer23', '0 to 32767', 0, 1, 'Q006');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer24', '0 to 65535', 1, 1, 'Q006');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer25', 'ASCII', 0, 1, 'Q007');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer26', 'ISO-LATIN-1', 0, 1, 'Q007');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer27', 'UNICODE', 1, 1, 'Q007');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer28', 'None of the mentioned', 0, 1, 'Q007');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer29', 'True & False', 1, 1, 'Q008');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer30', '0 & 1', 0, 1, 'Q008');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer31', 'Any integer value', 0, 1, 'Q008');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer32', 'true', 0, 1, 'Q008');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer33', 'ASCII', 0, 1, 'Q009');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer34', 'ISO-LATIN-1', 0, 1, 'Q009');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer35', 'None of the mentioned', 0, 1, 'Q009');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer36', 'ASCII and ISO-LATIN1', 1, 1, 'Q009');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer37', 'boolean b1 = 1;', 0, 1, 'Q010');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer38', 'boolean b2 = ‘false’;', 0, 1, 'Q010');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer39', 'boolean b3 = false;', 1, 1, 'Q010');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer40', 'boolean b4 = ‘true’', 0, 1, 'Q010');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer41', '8', 1, 1, 'Q011');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer42', '4', 0, 1, 'Q011');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer43', '2', 0, 1, 'Q011');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer44', '1', 0, 1, 'Q011');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer45', 'System.Int16', 0, 1, 'Q012');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer46', 'System.UInt32', 1, 1, 'Q012');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer47', 'System.UInt64', 0, 1, 'Q012');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer48', 'System.UInt16', 0, 1, 'Q012');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer49', 'int a = 32, b = 40.6;', 0, 1, 'Q013');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer50', 'int a = 42; b = 40;', 0, 1, 'Q013');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer51', 'int a = 32; int b = 40;', 1, 1, 'Q013');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer52', 'int a = b = 42;', 0, 1, 'Q013');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer53', 'long < short < int < sbyte', 0, 1, 'Q014');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer54', 'sbyte < short < int < long', 1, 1, 'Q014');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer55', 'short < sbyte < int < long', 0, 1, 'Q014');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer56', 'short < int < sbyte < long', 0, 1, 'Q014');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer57', 'sbyte', 1, 1, 'Q015');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer58', 'short', 0, 1, 'Q015');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer59', 'int', 0, 1, 'Q015');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer60', 'long', 0, 1, 'Q015');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer61', '10 Bytes', 0, 1, 'Q016');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer62', '6 Bytes', 0, 1, 'Q016');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer63', '4 Bytes', 1, 1, 'Q016');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer64', '8 Bytes', 0, 1, 'Q016');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer65', 'c = a + b;', 0, 1, 'Q017');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer66', 'c = a + int(float(b));', 0, 1, 'Q017');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer67', 'c = a + convert.ToInt32(b);', 1, 1, 'Q017');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer68', 'c = int(a + b);', 0, 1, 'Q017');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer69', 'Long Int', 0, 1, 'Q018');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer70', 'Unsigned Long', 0, 1, 'Q018');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer71', 'Int', 1, 1, 'Q018');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer72', 'Unsigned Int', 0, 1, 'Q018');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer73', 'float somevariable = 12.502D', 0, 1, 'Q019');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer74', 'float somevariable = (Double) 12.502D', 0, 1, 'Q019');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer75', 'float somevariable = (float) 12.502D', 1, 1, 'Q019');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer76', 'float somevariable = (Decimal)12.502D', 0, 1, 'Q019');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer77', 'Upto 6 digit', 0, 1, 'Q020');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer78', 'Upto 8 digit', 0, 1, 'Q020');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer79', 'Upto 9 digit', 0, 1, 'Q020');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer80', 'Upto 7 digit', 1, 1, 'Q020');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer81', 'COUNT()', 0, 1, 'Q021');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer82', 'NUMBER()', 0, 1, 'Q021');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer83', 'SUM()', 0, 1, 'Q021');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer84', 'COUNT(*)', 1, 1, 'Q021');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer85', 'MOST', 0, 1, 'Q022');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer86', 'TOP', 0, 1, 'Q022');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer87', 'MAX', 1, 1, 'Q022');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer88', 'UPPER', 0, 1, 'Q022');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer89', 'DELETE', 1, 1, 'Q023');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer90', 'REMOVE', 0, 1, 'Q023');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer91', 'DROP', 0, 1, 'Q023');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer92', 'CLEAR', 0, 1, 'Q023');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer93', 'DELETA', 0, 1, 'Q024');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer94', 'REMOVE', 0, 1, 'Q024');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer95', 'DROP', 0, 1, 'Q024');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer96', 'TRUNCATE', 1, 1, 'Q024');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer97', 'UPDATE', 1, 1, 'Q025');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer98', 'TRUNCATE', 0, 1, 'Q025');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer99', 'ALTER', 0, 1, 'Q025');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer100', 'None of the Mentioned', 0, 1, 'Q025');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer101', 'UPDATE and TRUNCATE', 0, 1, 'Q026');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer102', 'SELECT and INSERT', 0, 1, 'Q026');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer103', 'GRANT and REVOKE', 0, 1, 'Q026');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer104', 'ROLLBACK and SAVEPOINT', 1, 1, 'Q026');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer105', 'TCL', 0, 1, 'Q027');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer106', 'SCL', 1, 1, 'Q027');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer107', 'DCL', 0, 1, 'Q027');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer108', 'DDL', 0, 1, 'Q027');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer109', 'ASC', 1, 1, 'Q028');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer110', 'DESC', 0, 1, 'Q028');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer111', 'There is no default value', 0, 1, 'Q028');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer112', 'None of the mentioned', 0, 1, 'Q028');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer113', 'DELETE does not free the space containing the table and TRUNCATE free the space containing the table', 1, 1, 'Q029');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer114', 'Both DELETE and TRUNCATE free the space containing the table', 0, 1, 'Q029');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer115', 'Both DELETE and TRUNCATE does not free the space containing the table', 0, 1, 'Q029');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer116', 'DELETE free the space containing the table and TRUNCATE does not free the space containing the table', 0, 1, 'Q029');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer117', 'The AS SQL clause is used change the name of a column in the result set or to assign a name to a derived column', 1, 1, 'Q030');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer118', 'The AS clause is used with the JOIN clause only', 0, 1, 'Q030');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer119', 'The AS clause defines a search condition', 0, 1, 'Q030');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer120', 'All of the mentioned', 0, 1, 'Q030');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer121', 'MVC Architecture', 0, 1, 'Q031');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer122', 'Decorator pattern', 0, 1, 'Q031');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer123', 'MVVM Architectural pattern', 1, 1, 'Q031');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer124', 'Observer Pattern', 0, 1, 'Q031');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer125', '(expression)', 0, 1, 'Q032');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer126', '{{expression}}', 1, 1, 'Q032');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer127', '{{{expression}}}', 0, 1, 'Q032');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer128', '[expression]', 0, 1, 'Q032');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer129', '{{ expression | filter1 | filter2 | ... }}', 1, 1, 'Q033');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer130', '{{ expression | {filter1} | {filter2} | ... }}', 0, 1, 'Q033');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer131', '{{ expression - {filter1} - {filter2} - ... }}', 0, 1, 'Q033');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer132', '{{ {filter1} | {filter2} | ...-expression}}', 0, 1, 'Q033');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer133', 'ng-init', 0, 1, 'Q034');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer134', 'ng-app', 1, 1, 'Q034');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer135', 'ngSrc', 0, 1, 'Q034');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer136', 'ng-start', 0, 1, 'Q034');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer137', 'lowercase', 0, 1, 'Q035');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer138', 'orderby', 0, 1, 'Q035');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer139', 'email', 1, 1, 'Q035');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer140', 'currency', 0, 1, 'Q035');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer141', 'ng-cloak', 0, 1, 'Q036');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer142', 'ng-bind', 0, 1, 'Q036');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer143', 'ng-model', 1, 1, 'Q036');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer144', 'ng-blur', 0, 1, 'Q036');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer145', 'using Model', 0, 1, 'Q037');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer146', 'using services', 1, 1, 'Q037');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer147', 'using factory', 0, 1, 'Q037');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer148', 'using $scope', 0, 1, 'Q037');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer149', 'Module', 0, 1, 'Q038');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer150', 'Controller', 0, 1, 'Q038');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer151', 'Service', 0, 1, 'Q038');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer152', 'View', 1, 1, 'Q038');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer153', 'Tag', 0, 1, 'Q039');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer154', 'Attribute', 0, 1, 'Q039');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer155', 'Class name', 0, 1, 'Q039');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer156', 'All of the above', 1, 1, 'Q039');

INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer157', 'ng-form', 1, 1, 'Q040');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer158', 'ng-include', 0, 1, 'Q040');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer159', 'ng-Class', 0, 1, 'Q040');
INSERT INTO `testing_system_database`.`answer` (`answer_id`, `content`, `is_true`, `status`, `question_id`) VALUES ('Answer160', 'ng-bind', 0, 1, 'Q040');

-- insert exam_question table

INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('1', 'Exam001', 'Q001', '1 2 3 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('2', 'Exam001', 'Q002', '2 3 4 1');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('3', 'Exam001', 'Q003', '3 4 1 2');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('4', 'Exam001', 'Q004', '4 1 2 3');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('5', 'Exam001', 'Q005', '4 3 2 1');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('6', 'Exam001', 'Q006', '3 2 1 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('7', 'Exam001', 'Q007', '2 1 3 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('8', 'Exam001', 'Q008', '1 4 3 2');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('9', 'Exam001', 'Q009', '3 4 1 2');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('10', 'Exam001', 'Q010', '1 4 3 2');

INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('11', 'Exam002', 'Q011', '1 2 3 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('12', 'Exam002', 'Q012', '2 3 3 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('13', 'Exam002', 'Q013', '3 4 1 2');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('14', 'Exam002', 'Q014', '1 3 2 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('15', 'Exam002', 'Q015', '4 2 3 1');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('16', 'Exam002', 'Q016', '3 1 2 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('17', 'Exam002', 'Q017', '2 1 3 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('18', 'Exam002', 'Q018', '1 3 2 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('19', 'Exam002', 'Q019', '2 4 3 1');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('20', 'Exam002', 'Q020', '3 2 1 4');

INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('21', 'Exam003', 'Q021', '1 3 2 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('22', 'Exam003', 'Q022', '2 1 3 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('23', 'Exam003', 'Q023', '3 2 1 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('24', 'Exam003', 'Q024', '4 2 3 1');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('25', 'Exam003', 'Q025', '1 4 3 2');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('26', 'Exam003', 'Q026', '1 3 4 2');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('27', 'Exam003', 'Q027', '4 2 1 3');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('28', 'Exam003', 'Q028', '2 4 3 1');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('29', 'Exam003', 'Q029', '3 1 2 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('30', 'Exam003', 'Q030', '4 1 3 2');

INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('31', 'Exam004', 'Q031', '3 1 2 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('32', 'Exam004', 'Q032', '4 1 3 2');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('33', 'Exam004', 'Q033', '2 1 3 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('34', 'Exam004', 'Q034', '1 2 4 3');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('35', 'Exam004', 'Q035', '4 3 1 2');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('36', 'Exam004', 'Q036', '1 2 4 3');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('37', 'Exam004', 'Q037', '2 1 3 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('38', 'Exam004', 'Q038', '4 2 3 1');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('39', 'Exam004', 'Q038', '2 1 3 4');
INSERT INTO `testing_system_database`.`exam_question` (`id`, `exam_id`, `question_id`, `choice_order`) VALUES ('40', 'Exam004', 'Q030', '1 4 3 2');